# Better Gitlab Runner

Better Gitlab Runner provides some improvements to the base GitLab Runner image to allow the container to be easily automatically scalable in Docker Swarm, Nomad, Kubernetes or other alternative workload orchestrators. Essentially, this is accomplished by introducing an init system and running Gitlab Runner under a process supervisor. Initialization handles automatically registering the runner, and the process supervisor gives us the opportunity to de-register the runner when the container/child process shuts down.

I have considered including my changes in the gitlab-runner repository, but I am not sure there is a large enough demand for this to be worth while yet.

## Installation

TBD

## Usage

TBD

## Support

Unless it's a problem specific to the automatic registration and deregistration of the runner, you should get support for GitLab Runner from GitLab. See the repository at [https://gitlab.com/gitlab-org/gitlab-runner](https://gitlab.com/gitlab-org/gitlab-runner)

## Roadmap

( ) Better Documentation

This is considered feature complete. Basic maintainance as required.

## Contributing

I am open to contributions, especially assistance with documentation at this point.

## Authors and acknowledgment

Devon Bagley <devon.bagley@gmail.com>

## License

MIT

## Project status

Under Maintainance
